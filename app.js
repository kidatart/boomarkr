angular.module('bookmarkr',[])
.factory(
  'bookmarkrStorage',
  [ function() {
    /* 
      On first load, I check if the 'bookmarkrStorage' key exists.
      If it doesn't, I create one with an empty array for use later.
    */
    if ( localStorage.getItem('bookmarkrStorage') === null ) {
      localStorage.setItem('bookmarkrStorage', JSON.stringify( ['https://google.com'] ) );
    }
    return {
      get: function() {
        /* 
          Here I store the list of bookmarks in localstorage under the 'bookmarkrStorage' key.
        */
        return JSON.parse( localStorage.getItem('bookmarkrStorage') );
      },
      save: function(list) {
        /* 
          Here I save the list of bookmarks stored in localstorage
        */
        localStorage.setItem('bookmarkrStorage', JSON.stringify(list) );
      }
    }
  }]
)
.controller(
  'bookmarkrController',
  ['$scope', '$http', 'bookmarkrStorage', function( $scope, $http, bookmarkrStorage ) {
    
    var bkmrkr = this;

    $scope.bookmarks = bookmarkrStorage.get();
    $scope.submitMsg = { class: 'default', msg:'' };
    $scope.toggleEdit = [];

    /* 
    
      validateBookmark:
      This method takes the submitted URL as an argument.
      Using the $http method I then check for a response code.
      I've deemed that anything with the status code of 200 is a valid URL and ok to save.

      If the URL passes this simple test, I then pass it to the addBookmark method.
      submitAlert passes a success message and css class to the view.
      The submit box is then cleared ready for the next URL to be entered.
    
    */
    bkmrkr.validateBookmark = function( url ) {
      
      if( url ) {

        /* 
        
          Using the built in $http method to make an XHR request with the provided URL.
        
        */
        $http({
          method: 'GET',
          url: url,
        }).then( function( response ) {
  
          if( response.status == 200 ) {
  
            bkmrkr.addBookmark( url );
            $scope.submitAlert = { class: 'success', msg: 'Thank you! -  Bookmark Saved!' };                                  
            $scope.url = '';
  
          }
  
        },function( error ) {
  
          /* 
          
            If the URL returns any types of errors we catch it and handle it here.
          
          */
          $scope.submitAlert = { class: 'danger', msg: 'This URL doesn\'t exist!' };          
  
        });

      }else{

        /* 
        
          If a URL isnt added to the field, the HTTP method isnt fired and the error message below displayed/
        
        */
        $scope.submitAlert = { class: 'warning', msg: 'Please enter a URL!' };

      }

    }

    /* 
    
      The addBookmark method takes URL as an argument.
      The URL is then appended to the existing array of bookmarks.
      I then call the bookmarkrStorage service which saves the updated list of links.
    
    */
    bkmrkr.addBookmark = function( url ) {
      $scope.bookmarks.push( url )
      bookmarkrStorage.save( $scope.bookmarks );
      $scope.bookmarks = bookmarkrStorage.get();      
    }

    /* 
    
      The editBookmark method is attached to a click handler in the view.
      I have a toggleEdit boolean which helps toggle between the text and edit field in the view.
      The method accepts two arguments, one is the index of the item we choose to edit and the other for the URL we wish to update to.
    
    */
    bkmrkr.editBookmark = function( index, url ) {
      $scope.toggleEdit[index] = !$scope.toggleEdit[index];
      $scope.bookmarks[index] = url;
      bookmarkrStorage.save( $scope.bookmarks );
      $scope.bookmarks = bookmarkrStorage.get(); 
    }

    /* 
    
      Similar to the methods above, the removeBookmark method updates the existing bookmarks $scope and then saves the results before updating the $scope again.
    
    */
    bkmrkr.removeBookmark = function( index ) {
      $scope.bookmarks.splice( index, 1 );
      bookmarkrStorage.save( $scope.bookmarks );
      $scope.bookmarks = bookmarkrStorage.get();  
    }

    // Pagination 
    $scope.currentPage = 0;
    $scope.pageOffset = 0;
    $scope.pageSize = 4;

    /* 
    
      I've put a variable watch here so the page count is updated on bookmark insertion.
    
    */
    $scope.$watch('bookmarks', function() {
      $scope.pageCount = Math.ceil( $scope.bookmarks.length / $scope.pageSize );
    });

    bkmrkr.previousPage = function(){
      $scope.currentPage = $scope.currentPage - 1;
      $scope.pageOffset -= $scope.pageSize;
    }

    bkmrkr.nextPage = function(){
      $scope.currentPage = $scope.currentPage + 1;
      $scope.pageOffset += $scope.pageSize;
    }

  }]
)